#
# Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>
#
# This file is part of nixos-config
#
# nixos-config is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# nixos-config is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with nixos-config. If not, see
# <https://www.gnu.org/licenses/>.
#
{ config, pkgs, ... }:
{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "user";
  home.homeDirectory = "/home/user";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  # Bash
  programs.bash = {
    enable = true;
    initExtra = "
    eval \"$(direnv hook bash)\"
    ";
    shellAliases = {
      ll = "ls -l";
      ns = "nix-shell";
      ar = "appimage-run";
      ec = "emacsclient";
      ee = "emacseditor";
      v = "nvim";
      m = "make";
    };
  };

  # Zsh
  programs.zsh = {
    enable = true;
    enableAutosuggestions = true;
    autocd = true; # auto cd if typed just dir path
    initExtra = "
    source /home/user/.p10k.zsh
    eval \"$(direnv hook zsh)\"
    ";
    shellAliases = {
      ll = "ls -l";
      ns = "nix-shell";
      ar = "appimage-run";
      ec = "emacsclient";
      ee = "emacseditor";
      v = "nvim";
      m = "make";
    };
    history = {
      size = 10000;
      path = "${config.xdg.dataHome}/zsh/history";
    };
    plugins = [
      {
        name = "powerlevel10k";
        src = pkgs.zsh-powerlevel10k;
        file = "share/zsh-powerlevel10k/powerlevel10k.zsh-theme";
      }
    ];
    oh-my-zsh = {
      enable = true;
      plugins = [
        "git"
        "rust"
        "golang"
        "gradle"
        "python"
        "poetry"
        "pip"
        "virtualenv"
        "mix"
        "npm"
        "jruby"
        "docker"
        "docker-compose"
        "tmux"
        "z"
        "fzf"
        "sudo" # 2x Esc to add sudo
        "dirhistory" # moving hotkeys
        "history"
        "jsontools"
        "thefuck"
      ];
      theme = "agnoster";
    };
  };

  #NeoVIM
  programs.neovim = {
    enable = true;
    viAlias = true; # vi is symlink
    extraConfig = "set relativenumber";
    plugins = with pkgs.vimPlugins; [
      vim-nix
    ];
  };

  # Firefox
  programs.firefox = {
    enable = true;
    profiles.default = {
      id = 0;
    };
    extensions = with pkgs.nur.repos.rycee.firefox-addons; [
      ublock-origin
      noscript
      multi-account-containers
      facebook-container
      # theme
      theme-nord-polar-night
    ];
  };

  # Git
  programs.git = {
    enable = true;
    aliases = {
      c = "commit";
      cm = "commit -m";
      ca = "commit --amend";
      cane = "commit --amend --no-edit";
      s = "status";
      # branches
      co = "checkout";
      cob = "checkout -b";
      sw = "switch";
      swc = "switch -c";
      # merging
      r = "rebase";
      m = "merge";
      # stash
      st = "stash";
      stp = "stash pop";
      stl = "stash list";
      sts = "stash save";
      sta = "stash apply";
      # blame
      b = "blame -w -M3 -- ";
      # log
      lg = "log --graph --oneline --decorate";
    };
  };

  programs.bat.enable = true;
  programs.tmux.enable = true;
  programs.fzf.enable = true;
  programs.zathura = {
    enable = true;
    extraConfig = "
      set selection-clipboard clipboard
    ";
  };
  programs.lf.enable = true;
  programs.nnn.enable = true;

  home.packages = with pkgs; [
    ## Symlink farm manager
    stow
    ## Developer Tools
    meld
    zeal
    direnv
    ## for zsh
    autojump
    thefuck
    zsh-powerlevel10k
    ### LSP and PL
    ccls
    poetry
    nodePackages.pyright
    go
    gopls
    clojure
    clojure-lsp
    elixir
    elixir_ls
    nodePackages.svelte-language-server
  ];
}
