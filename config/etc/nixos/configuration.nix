#
# Copyright (C) 2022-2023 Kostiantyn Klochko <kostya_klochko@ukr.net>
#
# This file is part of nixos-config
#
# nixos-config is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# nixos-config is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with nixos-config. If not, see
# <https://www.gnu.org/licenses/>.
#
{ config, pkgs, ... }:
let
  unstable = import
    (builtins.fetchTarball https://github.com/nixos/nixpkgs/tarball/nixos-unstable)
    # reuse the current configuration
    { config = config.nixpkgs.config; };
in {
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Bootloader.
  # boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;
  boot.loader.efi.efiSysMountPoint = "/boot/efi";
  boot.loader.grub = {
    efiSupport = true;
    enable = true;
    version = 2;
    devices = [ "nodev" ];
    useOSProber = true;
  };
  
  boot.kernelModules = [ "kvm-amd" ]; # if you have Intel use "kvm-intel"

  networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  
  # Configure network proxy if necessary
  # networking.proxy.default = "http://user:password@proxy:port/";
  # networking.proxy.noProxy = "127.0.0.1,localhost,internal.domain";
  
  # Enable networking
  networking.networkmanager.enable = true;

  # Set your time zone.
  time.timeZone = "Europe/Kyiv";

  # Select internationalisation properties.
  i18n.defaultLocale = "en_US.utf8";
  
  i18n.extraLocaleSettings = {
    LC_ADDRESS = "uk_UA.utf8";
    LC_IDENTIFICATION = "uk_UA.utf8";
    LC_MEASUREMENT = "uk_UA.utf8";
    LC_MONETARY = "uk_UA.utf8";
    LC_NAME = "uk_UA.utf8";
    LC_NUMERIC = "uk_UA.utf8";
    LC_PAPER = "uk_UA.utf8";
    LC_TELEPHONE = "uk_UA.utf8";
    LC_TIME = "en_US.utf8"; #"uk_UA.utf8";
  };

  # Enabling PipeWire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    # If you want to use JACK applications, uncomment this
    #jack.enable = true;
  };

  # Configure keymap in X11
  services.xserver = {
    enable = true;
  
    # Touchpad
    libinput.enable = true;
  
    # Layouts
    layout = "us,ua"; # keyboard layouts
    xkbVariant = "us";
    xkbOptions = "grp:shifts_toggle"; # switching keyboard layouts
  
    displayManager = {
      sddm.enable = true;
      defaultSession = "none+awesome";
    };
  
    windowManager.awesome = {
      enable = true;
      luaModules = with pkgs.luaPackages; [
        luarocks
        luadbi-mysql
      ];
    };
  };

  # Add Light for changing the light setting.
  programs.light.enable = true;

  # Add NUR
  nixpkgs.config.packageOverrides = pkgs: {
    nur = import (builtins.fetchTarball "https://github.com/nix-community/NUR/archive/master.tar.gz") {
      inherit pkgs;
    };
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.user = {
    isNormalUser = true;
    description = "User";
    extraGroups = [ "networkmanager" "wheel" "audio" "video" "libvirtd" ];
    packages = with pkgs; [];
    shell = pkgs.zsh; # user's default shell
  };

  # List packages installed in system profile. To search, run:
  # $ nix search wget
  environment.systemPackages = with pkgs; [
    ## Editors
    # The Nano editor is installed by default.
    #emacs
    # my emacs configuration
    (import /home/user/.emacs.d/default.nix { inherit pkgs; })
    vim
    neovim
    ## System & Tools
    gawk
    ncdu
    wget
    yt-dlp
    tree
    htop
    powertop
    neofetch
    ffmpeg
    appimage-run
    xorg.xkill
    xscreensaver
    polybar
    trash-cli
    xclip
    ## Developer Tools
    git
    ## File Managers
    mc
    nnn
    ranger
    doublecmd # from 21.11
    ## Menu applications
    dmenu
    rofi
    ## Browsers
    firefox
    nyxt
    ## Terminals
    sakura
    alacritty
    kitty
    ## Security
    keepassxc
    ## Office
    logseq
    zathura
    libreoffice
    ## Science
    octave
    gnuplot
    ## Internet
    mumble
    element-desktop
    kdeconnect
    rsync
    ## Graphics
    feh # image viewer and wallpaper setter
    sxiv # image viewer
    flameshot
    gimp
    blender
    ## Video
    mpv
    freetube
    ## Audio
    audacious
    nuclear
    libsForQt5.kmix # for sound mixer
    pulsemixer
    ## Virtualisation
    qemu
    virt-manager
    ebtables
    podman-compose
    distrobox
    ## NixOS tools
    home-manager
  ];

  fonts.fonts = with pkgs; [
    ## Fonts
    powerline-fonts
    nerdfonts
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.mtr.enable = true;
  # programs.gnupg.agent = {
  #   enable = true;
  #   enableSSHSupport = true;
  # };

  # List services that you want to enable:
  
  # Virtualisation
  virtualisation = {
    podman = {
      enable = true;
      # alias docker=podman
      dockerCompat = true;
      # containers under podman-compose need it for talk to each other.
      defaultNetwork.dnsname.enable = true;
    };
    libvirtd = {
      enable = true;
      qemu = {
        ovmf.enable = true;
        runAsRoot = false;
      };
      onBoot = "ignore";
      onShutdown = "shutdown";
      extraOptions  = [ "--iptables=False" ];
    };
  };
  
  # Enable flatpak
  services.flatpak.enable = true;
  xdg.portal.extraPortals = [ pkgs.xdg-desktop-portal-gtk ];
  xdg.portal.enable = true;
  
  # Enable auto-cpufreq
  services.auto-cpufreq.enable = true;
  
  # Emacs
  services.emacs.enable = true;
  services.emacs.package = import /home/user/.emacs.d { pkgs = pkgs; };
  
  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # This value determines the NixOS release from which the default
  # settings for stateful data, like file locations and database versions
  # on your system were taken. It‘s perfectly fine and recommended to leave
  # this value at the release version of the first install of this system.
  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "22.11"; # Did you read the comment?
}
